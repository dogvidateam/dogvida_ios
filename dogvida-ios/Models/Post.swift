//
//  Post.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/18/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct Post: Decodable {
    var id: String
    var author: Author
    var commentCount: Int
    var detail: String
    var likeCount: Int
    var time: Double
    var title: String
}

struct Author: Decodable {
    var name: String
}
