//
//  PetCell.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/17/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class PetCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    var petName: String?
    
    func setCell() {
        if let petName = petName {
            nameLabel.text = petName
        }
    }
    
}
