//
//  PostCell.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/8/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class ImagePostCell: UITableViewCell {

    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var imageHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var textBottomConstraintConstant: NSLayoutConstraint!
    
    var post: Post?
    var delegate: UIViewController?
    
    func setCell() {
        guard let post = post else { return }
        self.profileImage.layer.cornerRadius = 8
        titleLabel.text = post.title
        let timeStamp = Date().convertTimestamp(serverTimestamp: post.time)
        timeLabel.text = timeStamp
        usernameLabel.text = "\(post.author.name)"
        profileImage.image = UIImage(named: "testPostImage")
        let likeString = post.likeCount == 1 ? "Like" : "Likes"
        likeCountLabel.text = "\(post.likeCount) \(likeString)"
        let commentString = post.commentCount == 1 ? "Comment" : "Comments"
        commentCountLabel.text = "\(post.commentCount) \(commentString)"
//        if let image = post.image {
//            postImage.image = UIImage(named: image)
//            imageHeightAnchor.constant = 157.0
//            textBottomConstraintConstant.constant = 8.0
//        } else {
            imageHeightAnchor.constant = 0.0
            textBottomConstraintConstant.constant = 0.0
//        }
    }
    @IBAction func imageTap(_ sender: Any) {
        showImage()
    }
    
    func showImage() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let imageDetailVC = storyBoard.instantiateViewController(withIdentifier: "ImageDetailVC") as! ImageDetailVC
        imageDetailVC.imageView = UIImageView(image: self.postImage.image)
        self.delegate?.present(imageDetailVC, animated: true, completion: nil)
    }
    
}
