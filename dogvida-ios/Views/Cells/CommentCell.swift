//
//  CommentCell.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/16/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var commentLabel: UILabel!
    var comment: String?
    
    func setCell() {
        if let comment = comment {
            commentLabel.text = comment
        }
    }
    
    @IBAction func likeButton(_ sender: Any) {
        print("\(comment)")
    }
    
}
