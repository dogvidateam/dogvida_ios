//
//  NotificationCell.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/20/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    
    var notification = ""
    
    func setCell() {
        self.titleLabel.text = notification
        self.timestampLabel.text = "1 week ago"
    }
    
}
