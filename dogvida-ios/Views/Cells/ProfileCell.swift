//
//  ProfileCell.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/17/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var dogNameLabel: UILabel!
    
    func setCell() {
        profileImageView.layer.cornerRadius = 8.0
        profileImageView.clipsToBounds = true
        usernameLabel.text = "Jake Sulkoske"
        dogNameLabel.text = "Winston"
    }
    
}
