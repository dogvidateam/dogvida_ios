//
//  SecondViewController.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/8/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class DiscoverVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var basic = ["Near me", "Recommended"]
    var following = ["Bulldog Health", "Dog Toys", "Vets"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        tableView.tableFooterView = UIView()
    }
    
    func setupNavBar() {
        self.navigationItem.title = "Discover"
        navigationController?.navigationBar.prefersLargeTitles = true
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }

}

extension DiscoverVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return basic.count
        }
        return following.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.accessoryType = .disclosureIndicator
        if indexPath.section == 0 {
            cell.textLabel?.text = basic[indexPath.row]
        } else {
            cell.textLabel?.text = following[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50.0))
        headerView.backgroundColor = .white
        let label = UILabel(frame: CGRect(x: 8, y: 8, width: tableView.bounds.size.width - 16, height: 34))
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        if section == 0 {
            label.text = "Suggested"
        } else {
            label.text = "Following"
        }
        headerView.addSubview(label)
        return headerView
    }
}

