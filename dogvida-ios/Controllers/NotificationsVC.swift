//
//  NotificationsVC.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/8/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class NotificationsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let notifications = ["@godfather_sage liked your post", "@raja_the_great commented on your post", "@winnie_da_bish commented on your post"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
    }
    
    func setupNavBar() {
        self.navigationItem.title = "Notifications"
        navigationController?.navigationBar.prefersLargeTitles = true
    }

}

extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1.0))
            headerView.backgroundColor = UIColor.groupTableViewBackground
            return headerView
        } else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.notification = notifications[indexPath.section]
        cell.setCell()
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}
