//
//  NewPostVC.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/18/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit
import Firebase

class NewPostVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var imageTappedButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var xButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    let vidaBaseUrl = "https://dogvida-fd14f.firebaseio.com/posts/"
    var placeholderLabel: UILabel!
    var imagePicker = UIImagePickerController()
    var delegate: HomeVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        postButton.isEnabled = false
        imageTappedButton.isEnabled = false
        imageView.layer.cornerRadius = 8.0
        xButton.isHidden = true
        textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Your words here"
        placeholderLabel.font = UIFont.systemFont(ofSize: (textView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
    }

    @IBAction func post(_ sender: Any) {
        guard let postText = textView.text else { return }
        self.showSpinner(backgroundColor: .spinner50Background, graySpinner: false)
        let postRef = Database.database().reference().child("posts").childByAutoId()
        let postObject = createPostObject(postText: postText, postRef: postRef.key)
        postRef.setValue(postObject) { (error, ref) in
            self.removeSpinner()
            guard error == nil else {
                print("ERROR POSTING: \(error!)")
                self.showPostingError()
                return
            }
            print("POST RES:")
            //self.delegate!.addPost(newPost: newPost)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showPostingError() {
        Toast.show(message: "My message", controller: self)
    }
    
    func createPostObject(postText: String, postRef: String) -> [String:Any] {
        let details = textView.text.isEmpty ? "" : textView.text
        return [
            "author":[
                "name":"jake"
            ],
            "commentCount":0,
            "detail": details!,
            "id":postRef,
            "likeCount":0,
            "selectedTags":[],
            "time":[".sv":"timestamp"],
            "title":postText,
            //UPDATE USER ID BELOW
            "userId":"jakesUserId"
        ] as [String:Any]
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func camera(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = false
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @IBAction func library(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[.editedImage] as? UIImage {
            DispatchQueue.main.async {
                self.imageView.image = image
                self.imageTappedButton.isEnabled = true
                self.xButton.isHidden = false
            }
        } else {
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            DispatchQueue.main.async {
                self.imageView.image = image
                self.imageTappedButton.isEnabled = true
                self.xButton.isHidden = false
            }
        }
        
    }
    
    @IBAction private func textFieldDidChange(_ sender: Any) {
        postButton.isEnabled = !titleTextField.text!.isEmpty
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    @IBAction func xTapped(_ sender: Any) {
        self.imageView.image = nil
        self.xButton.isHidden = true
        imageTappedButton.isEnabled = false
    }
    
    @IBAction func imageTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let imageDetailVC = storyBoard.instantiateViewController(withIdentifier: "ImageDetailVC") as! ImageDetailVC
        imageDetailVC.imageView = UIImageView(image: self.imageView.image)
        self.present(imageDetailVC, animated: true, completion: nil)
    }
    
}
