//
//  ImageDetailVC.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/18/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

class ImageDetailVC: UIViewController {
    
    var image: UIImage?
    var imageView: UIImageView!
    var scrollView: UIScrollView!
    var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        setZoomScale(for: scrollView.bounds.size)
        scrollView.zoomScale = scrollView.minimumZoomScale
        recenterImage()
    }
    
    func setupScrollView() {
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.backgroundColor = .darkText
        scrollView.contentSize = imageView.bounds.size
        scrollView.delegate = self
        scrollView.addSubview(imageView)
        view.addSubview(scrollView)
    }
    
    func setZoomScale(for scrollViewSize: CGSize) {
        let imageSize = imageView.bounds.size
        let widthScale = scrollViewSize.width / imageSize.width
        let heightScale = scrollViewSize.height / imageSize.height
        let minimumScale = min(widthScale, heightScale)
        scrollView.minimumZoomScale = minimumScale
        scrollView.maximumZoomScale = 3.0
    }
    
    func recenterImage() {
        let scrollViewSize = scrollView.bounds.size
        let imageViewSize = imageView.frame.size
        let horizontalSpace = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2.0 : 0
        let verticalSpace = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2.0 : 0
        let extra: CGFloat
        if navigationController != nil {
            extra = navigationController!.navigationBar.bounds.size.height
        } else {
            extra = 0
        }
        scrollView.contentInset = UIEdgeInsets(top: verticalSpace - extra, left: horizontalSpace, bottom: verticalSpace, right: horizontalSpace)
    }
    
    func setHeightForImage(image: UIImage) -> CGFloat {
        let imageWidth = image.size.width
        let imageHeight = image.size.height
        let scale = view.frame.width / CGFloat(imageWidth)
        let newImageHeight = CGFloat(imageHeight) * scale
        return newImageHeight
    }
    
    @IBAction func panGesture(_ sender: UIPanGestureRecognizer) {
        let touchpoint = sender.location(in: self.view?.window)
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchpoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchpoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchpoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchpoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
}

extension ImageDetailVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        recenterImage()
    }
}
