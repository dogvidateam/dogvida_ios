//
//  LoginVC.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 11/5/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class LoginVC: UIViewController {

    var delegate: HomeVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        KeychainWrapper.standard.set("jakesUserId123", forKey: "userId")
        delegate?.getPosts()
        self.dismiss(animated: true, completion: nil)
    }
    
}
