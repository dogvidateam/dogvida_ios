//
//  ProfileVC.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/8/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ProfileVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let pets = ["Winston", "Raja", "Brad"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupTableView()
    }
    
    func setupNavBar(){
        //self.navigationItem.title = "Profile"
        //navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "settingsIcon"), style: .plain, target: self, action: #selector(settingsTapped))
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }
    
    @objc func settingsTapped() {
        print("Settings")
    }
    
    func setupTableView(){
        tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        tableView.register(UINib(nibName: "PetCell", bundle: nil), forCellReuseIdentifier: "PetCell")
        tableView.register(UINib(nibName: "LogoutCell", bundle: nil), forCellReuseIdentifier: "LogoutCell")
        tableView.tableFooterView = UIView()
    }
    
    @objc func logoutAction() {
        KeychainWrapper.standard.remove(key: "userId")
        self.performSegue(withIdentifier: "unwindToRoot", sender: self)
        guard let homeVC = self.tabBarController?.viewControllers?.first?.children[0] as? HomeVC else { return }
        homeVC.navigationController?.popViewController(animated: true)
        homeVC.handleAccess()
    }

}

extension ProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return pets.count
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
            cell.setCell()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PetCell", for: indexPath) as! PetCell
            cell.petName = pets[indexPath.row]
            cell.setCell()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 210
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 1 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50.0))
            headerView.backgroundColor = .white
            let label = UILabel(frame: CGRect(x: 8, y: 8, width: tableView.bounds.size.width - 16, height: 34))
            label.textAlignment = NSTextAlignment.left
            label.text = "Dogs"
            label.font = label.font.withSize(20)
            label.backgroundColor = .white
            headerView.addSubview(label)
            return headerView
        } else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50.0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
            let logoutButton = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 50))
            logoutButton.backgroundColor = .white
            logoutButton.setTitle("Log Out", for: .normal)
            logoutButton.setTitleColor(.systemBlue, for: .normal)
            logoutButton.addTarget(self, action: #selector(logoutAction), for: .touchUpInside)
            logoutButton.layer.cornerRadius = 15.0
            logoutButton.center = footerView.center
            footerView.addSubview(logoutButton)
            return footerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 70
        } else {
            return 0
        }
    }
    
}
