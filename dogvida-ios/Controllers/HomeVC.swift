//
//  FirstViewController.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/8/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import FirebaseDatabase
import CodableFirebase

class HomeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var objects: Data?
    //var posts = [Post(id: "1", authorName: "voodoomamajuju", authorImage: "testProfileImage", timeStamp: "January 1, 2019", title: "Can dogs drink tequila?", image: "testPostImage", likeCount: 34, commentCount: 2), Post(id: "2", authorName: "neilJain56", authorImage: "testProfileImage", timeStamp: "June 23, 2019", title: "What is the best dog toy for Raja?", image: nil, likeCount: 206, commentCount: 57), Post(id: "3", authorName: "voodoomamajuju", authorImage: "testProfileImage", timeStamp: "May 15, 2019", title: "It is Winston's birthday! What treats should I get him?", image: nil, likeCount: 108, commentCount: 14), Post(id: "4", authorName: "voodoomamajuju", authorImage: "testProfileImage", timeStamp: "September 1, 2019", title: "What's up, pups?", image: "testProfileImage", likeCount: 12, commentCount: 0)]
    var posts = [Post]()
    var userId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleAccess()
        tableView.register(UINib(nibName: "ImagePostCell", bundle: nil), forCellReuseIdentifier: "ImagePostCell")
        setupNavBar()
    }
    
    func handleAccess() {
        guard let userId = KeychainWrapper.standard.string(forKey: "userId") else {
            //Show login screen
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginVC.delegate = self
            loginVC.modalPresentationStyle = .fullScreen
            DispatchQueue.main.async {
                self.navigationController?.present(loginVC, animated: true, completion: nil)
            }
            return
        }
        self.userId = userId
        getPosts()
    }
    
    func getPosts() {
        self.showSpinner(backgroundColor: .clear, graySpinner: true)
        Database.database().reference().child("posts").observeSingleEvent(of: .value, with: { (snapshot) in
            self.removeSpinner()
            guard let value = snapshot.value else { return }
            print("VALUE: \(value)")
            do {
                let responseObject = try FirebaseDecoder().decode([String: Post].self, from: value)
                self.manageData(data: responseObject)
            } catch let error {
                //Show error message could not get posts
                print(error)
            }
        })
    }
    
    func setupNavBar(){
        self.navigationItem.title = "Trending"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "newPostIcon"), style: .plain, target: self, action: #selector(createTapped))
    }
    
    func manageData(data: [String: Post]) {
        var tempPosts = [Post]()
        for value in data.values {
            tempPosts.append(value)
        }
        tempPosts.sort {
            $0.time > $1.time
        }
        self.posts += tempPosts
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func addPost(newPost: Post) {
        self.posts.insert(newPost, at: 0)
        self.tableView.reloadData()
    }
    
    @objc func createTapped() {
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newPostVC = mainStoryboard.instantiateViewController(withIdentifier: "NewPostVC") as! NewPostVC
        newPostVC.delegate = self
        newPostVC.modalPresentationStyle = .fullScreen
        DispatchQueue.main.async {
            self.navigationController?.present(newPostVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    
}

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1.0))
        headerView.backgroundColor = UIColor.groupTableViewBackground
        return headerView

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImagePostCell", for: indexPath) as! ImagePostCell
        cell.delegate = self
        cell.post = posts[indexPath.section]
        cell.setCell()
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let postDetailVC = mainStoryboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        postDetailVC.post = posts[indexPath.section]
        self.navigationController?.pushViewController(postDetailVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
