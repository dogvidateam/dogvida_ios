//
//  DataExtension.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 10/14/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import Foundation

extension Date {
    
    func convertTimestamp(serverTimestamp: Double) -> String {
        let x = serverTimestamp / 1000
        let date = NSDate(timeIntervalSince1970: x)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, h:mm"
        return formatter.string(from: date as Date)
    }
    
}
