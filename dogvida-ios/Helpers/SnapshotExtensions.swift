//
//  SnapshotExtensions.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 10/2/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import Foundation
import FirebaseDatabase

extension DataSnapshot {
    func decoded<T: Decodable>() throws -> T {
        let jsonData = try JSONSerialization.data(withJSONObject: self.children, options: [])
        let object = try JSONDecoder().decode(T.self, from: jsonData)
        return object
    }
}

