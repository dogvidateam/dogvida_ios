//
//  ColorsExtension.swift
//  dogvida-ios
//
//  Created by Jake Sulkoske on 9/17/19.
//  Copyright © 2019 Jake Sulk. All rights reserved.
//

import UIKit

extension UIColor {
    static let vidaTheme = UIColor(red: 171.0, green: 71.0, blue: 188.0, alpha: 1.0)
    static let spinner50Background = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
}
